# Sterling Analytics Setup Tool
### Please follow the instructions below

1. Use the terminal to enter the command below
  > ```chmod +x ./SterlingAnalytics.sh && ./SterlingAnalytics.sh```
2. Follow along with the menu prompt; type `start` and hit enter
3. When the *Authorize Cloud Shell* comes up, hit "Authorize" 
4. Open <walkthrough-editor-spotlight spotlightId="file-explorer">[sterling_enabled_sites.sh](../../sterling_enabled_sites.sh)</walkthrough-editor-spotlight>
5. Uncomment the `# echo my-site-example` lines for each site you want to generate Sterling service accounts for.
6. Click on the terminal window and press any key.
7. A file called <walkthrough-editor-spotlight spotlightId="file-explorer">'send_me_to_sterling.zip'</walkthrough-editor-spotlight> will be created; Right-click it, select Download
8. Email the file as an attachment to 
  <a href="mailto:sean.price@blvdagency.com?subject=SterlingKeys">sean.price@blvdagency.com</a>
  

Go to the <a href="https://marketingplatform.google.com/home/accounts?product=analytics" target="_blank">Google Marketing Platform Console</a> and add the service account as a "Read & Analyze" user to each dealer site on the Sterling Platform.
___

## About This Tool
This tool was built to quickly enable the Google Analytics Reporting Api and create a service account (sterling-analytics-sa) for each dealer site on the Sterling platform. 

Please feel free to view the contents of [SterlingAnalytics.sh](./SterlingAnalytics.sh) if you want to know specifically what is being run. Below  is a breakdown of the [gcloud](https://cloud.google.com/sdk/gcloud/reference) commands this tool uses. Click any of the commands to go to their documentation.

- [```gcloud projects list```](https://cloud.google.com/sdk/gcloud/reference/projects/list)
  - Used to get a list of google projects the current user has access to.
  - Those items get inserted into `sterling_enabled_sites.sh` so that individual sites can be chosen.
  - This is also used by the `remove` menu option, but only to look for projects that have a "sterling-analytics-sa" service account on them so they can be removed
- [```gcloud config set project```](https://cloud.google.com/sdk/gcloud/reference/config/set#project)
