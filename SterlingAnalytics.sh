declare SCRIPT_VERSION='1.0'
STERLING_KEY_DIRECTORY=./Sterling_Keys
ENABLED_SITES_FILE=~/sterling_enabled_sites.sh

handle_enabled_sites_selection () {
    any_key_prompt "${teal}Once you have uncommented the projects you would like to select, press any key to continue.${normal}";
    mapfile -t ENABLED_SITES < <($ENABLED_SITES_FILE);

    if ((${#ENABLED_SITES[@]})); then
        printf '\n%s' "The Google Reporting API will be enabled and Sterling Service accounts and keys will be made for the following project IDs:"\
        "${ENABLED_SITES[@]}"\
        "" "Would you like to continue? (y/n)";
        read -n 1 answer

        case $answer in 
            y | Y)
                for id in "${ENABLED_SITES[@]}"
                do
                    unset_gcloud_project &&
                    set_gcloud_project $id &&
                    enable_ga_reporting_api &&
                    add_sterling_account_for_proj $id &&
                    unset_gcloud_project;
                done

                zip -r send_me_to_sterling.zip ./Sterling_Keys

                printf '\n%s' "" "${green}All Set!${normal}" ""\
                "You'll see ${purple}send_me_to_sterling.zip${normal} in the file explorer to the left."\
                "Send that as an email attachment to ${teal}sean.price@blvdagency.com${normal} with the subject 'Sterling Keys'"\
                "" ""
            ;;
            n | N)
                exit_prompt;
            ;;
            *)
               handle_enabled_sites_selection;
            ;; 
            esac
    else
        printf '\n%s' "${red}No projects were selected.${normal}"\
                "Please uncomment at least one project from sterling_enabled_sites.sh to continue."\
                "If you wish to exit, press ctrl + c" ""
        handle_enabled_sites_selection
    fi
}

create_sterling_enabled_sites_script () {
    local site_id
    local site_name
    echo "#####!!!!!" > $ENABLED_SITES_FILE
    printf "\n%s" "# The items below are the Project IDs and display names for projects in your organization." "# Remove the '#' in the second line (with 'echo') for each project you want to create a Sterling service account for." "" "#####!!!!!" ""  >> $ENABLED_SITES_FILE;
    printf "\n"
    gcloud projects list | awk -v FS="  +|\t" -v end=NF-1 {' if(NR!=1) printf("\n### Project Name: %s \n# echo %s \n###################\n", $2, $1) '} >> $ENABLED_SITES_FILE;
    printf "\n"
    chmod +x $ENABLED_SITES_FILE;
    clear;
    printf "\n\nNow please open ${teal}sterling_enabled_sites.sh${normal} in the editor.\nYou should see it in the file explorer on the left.\n\n"
    handle_enabled_sites_selection
}

set_gcloud_project() {
    gcloud config set project $1
}

enable_ga_reporting_api () {
    gcloud services enable analyticsreporting.googleapis.com
}

add_sterling_account_for_proj() { 
    account_name="sterling-analytics-sa"
    gcloud iam service-accounts create $account_name \
        --description="Sterling analytics program service account for project $1" \
        --display-name="Sterling Analytics" &&
    gcloud projects add-iam-policy-binding $1 \
        --member="serviceAccount:$account_name@$1.iam.gserviceaccount.com" \
        --role="roles/viewer" &&
    gcloud iam service-accounts keys create ~/$STERLING_KEY_DIRECTORY/$1.json --iam-account $account_name@$1.iam.gserviceaccount.com
}

unset_gcloud_project () {
    gcloud config unset project;
}

remove_all_service_accounts() {
    for PROJECT in $(gcloud projects list --format='value(projectId)')
    do
        for ACCOUNT in $(gcloud iam service-accounts list --project=${PROJECT} --format='value(email)' --filter='email ~ sterling-analytics-sa')
        do
            gcloud iam service-accounts delete $ACCOUNT --quiet
        done
    done
}

print_main_menu () {
    printf '\n%s'\ ""
    if [[ -f $ENABLED_SITES_FILE ]];then
        printf '\n%s' "${green}resume${normal} - Resumes setup using sterling_enabled_sites.sh"
    fi
    printf '\n%s'\
    "${teal}start${normal} - Begins setup"\
    "${teal}remove${normal} - Remove Sterling service accounts from individual project"\
    "${teal}remove_all${normal} - Bulk remove all Sterling service accounts"\
    "${teal}exit${normal} - Quits setup tool"\
    ""
}

any_key_prompt () {
    printf "\n\n"
    read -n 1 -s -r -p "$@"
    printf "\n\n"
}

print_remove_option_warning () {
    printf '\n%s' "${red}NOTE: ${normal}Both remove options do not disable Google Analytics Reporting API as it could potentially be used by another service."\
        "      You can disable APIs through the Google Cloud Console here:"\
        "      https://console.cloud.google.com/apis"\
        "      or via gcloud terminal: "\
        "      https://cloud.google.com/endpoints/docs/openapi/enable-api"\
        "      (using 'disable' instead of 'enable' in the example)"
}

clean_up () {
    rm $ENABLED_SITES_FILE
    rm -r $STERLING_KEY_DIRECTORY
}

reopen_main_menu () {
    print_main_menu;
    handle_main_menu;
}

handle_main_menu () {
    echo "Type your command:"
    read MENU_OPTION

    default_prompt() {
        printf '\n%s' ""\
            "${red} '$MENU_OPTION' ${normal}is not an option."\
            "Please select one of the following:";
            reopen_main_menu
    }
    case $MENU_OPTION in
        resume)
            if [[ -f $ENABLED_SITES_FILE ]];then
                handle_enabled_sites_selection
            else
                default_prompt
            fi
        ;;
        start)
            create_sterling_enabled_sites_script
        ;;
        remove)
            echo "Not available at the moment"
        ;;
        remove_all)
            echo 
            read -p "This will remove all Sterling service accounts. Would you like to continue? (y/n)" -n 1 -r
            echo
            if [[ $REPLY =~ ^[Yy]$ ]]
            then 
                remove_all_service_accounts
            else
                reopen_main_menu
            fi
        ;;
        clean)
            printf '\n%s' ;
            read -p "This will delete any of the files this script created. Would you like to continue? (y/n)" -n 1 -r
            echo
            if [[ $REPLY =~ ^[Yy]$ ]]
            then 
                clean_up
            else
                reopen_main_menu
            fi
        ;;
        exit)
            clear;
            exit_prompt;
        ;;
        *)
            default_prompt;
        ;;
        esac
}

welcome_prompt () {
    local pink=$(tput setaf 5)
    local blue=$(tput setaf 4)
    local teal=$(tput setaf 6)
    local red=$(tput setaf 1)
    local green=$(tput setaf 2)
    local normal=$(tput sgr0)
    clear;

    printf '\n%s' "" \
        "${pink}//// Sterling Analytics Setup Tool (v.${SCRIPT_VERSION}) ////" "" \
        "${blue}This tool will help you enable the Google Analytics Reporting API and create service account keys\
 for each dealer site on the Sterling Platform that you manage."\
        "" "" "${normal}"
        
    any_key_prompt "Press any key to go to the menu."

    clear;

    printf '\n%s' ""\
        "${blue}Here are the commands available:";

    print_main_menu;
    print_remove_option_warning;

    printf '\n%s' "${normal}" "";

    handle_main_menu;
}

exit_prompt () {
    clear;
    printf '\n%s' "" ""\
        "To start this tool again, just type ${green}~/SterlingAnalytics.sh${normal} and press Enter."\
        "" "Exiting..." "" ""
    exit 0
}

welcome_prompt